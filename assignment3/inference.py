import sys
import shlex
import copy

functions = dict()

class KnowledgeBase:
    def __init__(self):
        global functions
        self.facts = []
        self.rules = {}
        self.functions = functions
        self.standard_var = 1
        self.record = dict()
        self.and_depth = 0
        self.or_depth = 0
        self.cantSolve = {}

    def add_rules(self, rule):
        new_rule = self.standardize_variables(rule, init = True)
        for lhs in new_rule.LHS:
            self.functions.update({lhs.identifier:lhs})
        self.functions.update({new_rule.RHS.identifier:new_rule.RHS})

        if self.rules.get(new_rule.RHS.functionName) == None:
            self.rules.update({new_rule.RHS.functionName:[new_rule]})
        else:
            self.rules.get(new_rule.RHS.functionName).append(new_rule)

    def standardize_variables(self, rule, init=False):
        standard_rule = copy.deepcopy(rule)
        functions = standard_rule.LHS + [standard_rule.RHS]
        variables = []
        variable_map = dict()
        for function in functions:
            for var in function.variables:
                if var[0].isupper() is False:
                    variable_map.update({var:0})

        for var in variable_map.keys():
            if variable_map.get(var) == 0:
                variable_map.update({var:self.standard_var})
                self.standard_var += 1

        for function in functions:
            for i, var in enumerate(function.variables):
                if var[0].isupper() is False:
                    function.variables[i] = variable_map.get(var)\
                            if init == False else\
                            function.variables[i] + str(variable_map.get(var))
        return standard_rule

    def tell(self, rule):
        self.add_rules(rule)

    def ask(self, query):
        g = self.bc_or(query, {})
        self.record = {}
        self.or_depth = self.and_depth = 0
        return next(g, False)

    def bc_or(self, goal, theta):
        self.or_depth += 1
        record = goal.__str__()# + theta.__str__()
        #print "bc_or", goal.__str__()
        #for r in self.record.keys():
        #    print r
        #raw_input()
        if self.record.get(record) != None:
            #print "loop!"
            #print record
            self.or_depth -= 1
            return
        self.record.update({record:True})

        for rule in self.selectRules(goal):
            new_rule = self.standardize_variables(rule)
            #new_rule.print_rule()
            and_g = self.bc_and(new_rule.LHS, \
                    self.unify(new_rule.RHS.variables, goal.variables, copy.deepcopy(theta)))

            while True:
                substitution = next(and_g, False)
                if substitution == False:
                    break
                else:
                    self.record.pop(record)
                    yield substitution
                    self.record.update({record:True})

        self.record.pop(record)
        self.or_depth -= 1


    def bc_and(self, goals, theta):
        #print "bc_and",
        #for g in goals:
        #    print g.__str__(),
        #print ""
        self.and_depth += 1
        if theta == False:
            self.and_depth -= 1
            return
        elif len(goals) is 0:
            yield theta
        else:
            for i in xrange(len(goals)):
                first = goals[i]
                rest = (goals[:])
                rest.pop(i)
                #first, rest = goals[i],goals[1:]
                or_g = self.bc_or(self.subst(theta, first), copy.deepcopy(theta))
                while True:
                    thetap = next(or_g, False)
                    if thetap is False:
                        break
                    else:
                        and_g = self.bc_and(rest, copy.deepcopy(thetap))
                        while True:
                            thetapp = next(and_g, False)
                            if thetapp is False:
                                break
                            else:
                                self.and_depth -= 1
                                yield copy.deepcopy(thetapp)
                                self.and_depth += 1

        self.and_depth -= 1

    def unify(self, x, y, theta):
        #print x,y,theta
        if theta == False:
            return False
        elif x == y:
            return theta
        elif type(x) is int:
            return self.unify_var(x, y, theta)
        elif type(y) is int:
            return self.unify_var(y, x, theta)
        elif type(x) is list and type(y) is list:
            return self.unify(x[1:], y[1:], self.unify(x[0], y[0], theta))
        else:
            return False

    def unify_var(self, var, x, theta):
        if theta.get(var) != None:
            return self.unify(theta.get(var), x, theta)
        elif theta.get(x) != None:
            return self.unify(var, theta.get(x), theta)
        else:
            theta.update({var:x})
            return theta

    def subst(self, theta, query):
        #print "====subst====="
        #print theta, query.variables
        return_query = copy.deepcopy(query)
        for i, var in enumerate(query.variables):
            if theta.get(var) != None:
                return_query.variables[i] = theta.get(var)
        return return_query

    def selectRulesGen(self, query):
        #print "=========rule selected==============="
        rules = self.rules.get(query.functionName)
        if rules is None:
            return
        for rule in rules:
            if rule.RHS.canUnifyWith(query):
                #rule.print_rule()
                yield rule
        #print "=========end ofrule selected==============="

    def selectRules(self, query):
        return sorted(list(self.selectRulesGen(query)), key=lambda x: len(x.LHS))

    def print_knowledgeBase(self):
        print("rules")
        for key in self.rules.keys():
            for r in self.rules.get(key):
                r.print_rule()
        #raw_input()

class function:
    def __init__(self, functionName, variables, truth):
        self.functionName = functionName
        self.variables = variables
        self.truth = truth

    def negate(self):
        negate = copy.copy(self)
        negate.truth = True ^ self.truth
        return negate

    def __eq__(self, other):
        return True if self.functionName == other.functionName\
                and self.truth == other.truth\
                and self.variables == other.variables else False

    def canUnifyWith(self, other):
        if self.functionName == other.functionName\
                and self.truth == other.truth:
            unifiable = True
            for i, k in enumerate(other.variables):
                if type(k) is str and type(self.variables[i]) is str\
                        and k[0].isupper() and self.variables[i][0].isupper()\
                        and k != self.variables[i]:
                    unifiable = False

            return unifiable
        else:
            return False

    def set_function_id(self, identifier):
        self.identifier = identifier

    def print_function(self):
        print self.identifier, self.truth, self.functionName, self.variables

    def __str__(self):
        global functions

        return_str = (str(self.truth) + " " + self.functionName)
        for i, variable in enumerate(self.variables):
            return_str += (" " + (str(variable) if type(variable) is not int else
                functions.get(self.identifier).variables[i]))
            return_str += (" " + str(self.identifier))
        return return_str


class rule:
    def __init__(self, functionsLeft, functionsRight):
        self.LHS = copy.deepcopy(functionsLeft)
        self.RHS = copy.deepcopy(functionsRight)

    def set_rule_id(self, num):
        self.identification = num

    def print_rule(self):
        if type(self.LHS) is not bool:
            for f in self.LHS:
                print f.truth, f.functionName, f.variables,
        else:
            print self.LHS,

        print " => ",
        f = self.RHS
        print f.truth, f.functionName, f.variables,
        print ""

class utility:
    search_log = {}
    def __init__(self):
        self.function_id = 0
    def processRule(self, functionStr):
        terms = list(shlex.shlex(functionStr))
        termProgress = 0
        parameterList = []
        LHS = []
        RHS = []
        truth = True
        self.function_id += 1

        functionList = []
#        print terms
        for term in terms:
            if termProgress == 0 and term == "~":
#                print "progress 0"
                truth = False
            elif termProgress == 0 and term[0].isupper():
#                print "progress 0"
                functionName = term
                termProgress = 2
            elif termProgress == 2 and term == "(":
#                print "progress 2"
                termProgress = 4
            elif termProgress == 4:
#                print "progress 4"
                if term[0].isupper() or term[0].islower():
                    parameterList.append(term)
                    termProgress = 5
            elif termProgress == 5 and term == ",":
#                print "progress 5"
                termProgress = 4
            elif termProgress == 5 and term == ")":
#                print "progress 5"
                self.function_id += 1
                new_function = function(functionName, parameterList, truth)
                new_function.set_function_id(self.function_id)
                for f in functionList:
                    if new_function.__eq__(f):
                        functionList.remove(f)
                        break
                functionList.append(new_function)
                functionName = None
                parameterList = []
                truth = True
                termProgress = 6
            elif termProgress == 6 and term == "^":
#                print "progress 6"
                termProgress = 0
            elif termProgress == 6 and term == "=":
#                print "progress 6"
                LHS = functionList
                functionList = RHS
                termProgress = 0

        new_rule = rule([], functionList[0]) if RHS == [] \
                    else rule(LHS, RHS[0])
        #new_rule.print_rule()

        return new_rule

if __name__ == "__main__":
    #global functions
    inputFile = sys.argv[2]
    kb = KnowledgeBase()
    queryList = []
    ruleList = []
    utility = utility()
    with open(inputFile) as f:
        lines = f.readlines()
        progress = 0
        for i, line in enumerate(lines):
            if progress == 0:
                queryNum = int(line)
                queryInit = 0
                progress = 1
            elif progress == 1:
                query = utility.processRule(line)
                queryList.append(query.RHS)
                functions.update({query.RHS.identifier:query.RHS})
                queryInit += 1
                if queryInit == queryNum:
                    progress = 2
            elif progress == 2:
                ruleNum = int(line)
                progress = 3
                ruleInit = 0
            elif progress == 3:
                kb.tell(utility.processRule(line))
                ruleInit += 1
                if ruleInit == ruleNum:
                    progress = 4
            else:
                break

        kb.print_knowledgeBase()
        #for f in kb.functions.keys():
        #    print f, kb.functions.get(f).__str__()
        with open("output.txt", 'w') as outputFile:
            for query in queryList:
                result = kb.ask(query)
                outputFile.write("FALSE\n" if result == False else "TRUE\n")
                print result
                #raw_input()


