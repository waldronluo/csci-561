import sys
import bisect
#import networkx as nx
#import matplotlab.pyplot as plt
#import pylab
from Queue import deque

debug_flag = 0
class Problem:
    def __init__(self, init_state, dests, links, start_time):
        self.initial_state = init_state
        self.build_links(links)
        self.source = init_state
        self.dests = dests
        self.start_time = start_time


    def compute_restrict_time(self, restricts):
        available_time = [x for x in range(24)]
        for restrict in restricts:
            begin = int(restrict.split('-')[0])
            end = int(restrict.split('-')[1])
            for i in range(begin, end+1):
                if i in available_time:
                    available_time.remove(i)
        return available_time

    def build_links(self, links):
        self.links = dict()
        for link in links:
            if self.links.get(link[0]) is None:
                if link[3] is 0:
                    self.links.update({link[0]: {link[1]: (int(link[2]), self.compute_restrict_time([]))}})
                else:
                    self.links.update({link[0]: {link[1]: (int(link[2]), self.compute_restrict_time(link[4:]))}})
            else:
                if link[3] is 0:
                    self.links.get(link[0]).update({link[1]: (int(link[2]), self.compute_restrict_time([]))})
                else:
                    self.links.get(link[0]).update({link[1]: (int(link[2]), self.compute_restrict_time(link[4:]))})
        ######
        # check this function when you open this file
        ######

    def actions(self, state, time_cost=0, search='BFS'):
        if search == 'BFS' or search =='DFS':

            actions = self.links.get(state)
            if actions is None:
                return None
            else:
                actions = actions.keys()
                actions.sort()
                if search == "DFS":
                    actions.reverse()
                return actions
        elif search == 'UCS':
            result_actions = []
            available_links = self.links.get(state)

            if available_links is None:
                return None

            for key in available_links.keys():
                available_time = available_links.get(key)[1]
                if (time_cost % 24) in available_time:
                    result_actions.append(key)
            return result_actions

    def goal_test(self, state):
        return state in self.dests

class Uniform_Cost_Search:
    class ucs_queue:
        def __init__(self, nodes):
            self.priority_queue = sorted(nodes, key=lambda x: (x.path_cost, x.state))
            self.hash_map = dict()
            for node in nodes:
                self.hash_map.update({node.state: node.path_cost})

        def append(self, node):
            position = bisect.bisect(self.priority_queue, node)
            bisect.insort(self.priority_queue, node)
            self.hash_map.update({node.state: node.path_cost})

        def get_path_cost(self, state):
            return self.hash_map.get(state)

        def remove(self, state):
            for node in self.priority_queue:
                if node.state == state:
                    self.priority_queue.remove(node)
            self.hash_map.pop(state)

        def replace_node(self, node):
            self.remove(node.state)
            self.append(node)

        def empty(self):
            return True if not self.priority_queue else False

        def pop(self):
            node = self.priority_queue.pop(0)
            self.hash_map.pop(node.state)
            return node


    class u_node:
        def __init__(self, state, path_cost=0):
            self.state = state
            self.path_cost = path_cost

        def __lt__(self, other):
            return (self.path_cost, self.state) < (other.path_cost, other.state)

    def state_in(self, frontier, state):
        return True if frontier.hash_map.get(state) is not None else None

    def child_node(self, problem, node, action):
        current_path_cost = node.path_cost
        action_path_cost = problem.links.get(node.state).get(action)[0]
        return self.u_node(action, current_path_cost + action_path_cost)

    def solution(self, node):
        return (node.state, node.path_cost % 24)

    def do_search(self, problem):
        self.problem = problem
        node = self.u_node(self.problem.initial_state, self.problem.start_time)
        self.frontier = self.ucs_queue([node])
        self.explored = set()
        while(True):
            #debug_line = "Explored: "
            #if self.explored is None:
            #    debug_line += "None"
            #else:
            #    for dnode in self.explored:
            #        debug_line = debug_line + " " + dnode[0]

            #debugLines.append(debug_line)

            #debug_line = "Priority_queue: "
            #if self.frontier.priority_queue == []:
            #    debug_line += "None"
            #else:
            #    for dnode in self.frontier.priority_queue:
            #        debug_line = debug_line + " " + "(" + dnode.state +"," + str(dnode.path_cost) + ")"

            #debugLines.append(debug_line)

            if self.problem.goal_test(node.state):
                return self.solution(node)
            if self.frontier.empty():
                return None

            self.explored.add(node.state)
            node = self.frontier.pop()
            actions = self.problem.actions(node.state, search="UCS", time_cost=node.path_cost)
            if actions is None:
                continue
            for action in actions:
                child = self.child_node(problem, node, action)
                if child.state not in self.explored and not self.state_in(self.frontier, child.state):
                    self.frontier.append(child)
                elif self.state_in(self.frontier, child.state) and\
                        self.frontier.get_path_cost(child.state) > child.path_cost:
                        self.frontier.replace_node(child)

class Breadth_First_Search:
    class b_node:
        def __init__(self, state, path_cost=0):
            self.state = state
            self.path_cost = path_cost

    def solution(self, node):
        return (node.state, (node.path_cost + self.problem.start_time) % 24) if node is not None else None


    def __init__(self):
        self.frontier = []
        self.explored = set()

    def child_node(self, problem, node, action):
        return self.b_node(action, node.path_cost + 1)

    def state_in(self, frontier, state):
        for node in self.frontier:
            if state == node.state:
                return True
        return False

    def do_search(self, problem):
        self.problem = problem
        node = self.b_node(problem.initial_state)
        if self.problem.goal_test(node.state) is True:
            return self.solution(node)
        self.frontier = [node]
        self.explored = set()
        while(True):
            if not self.frontier:
                return None
            node = self.frontier.pop(0)
            if self.problem.goal_test(node.state) is True:
                return self.solution(node)
            self.explored.add(node.state)
            actions = self.problem.actions(node.state)
            frontier_extend = []
            if actions is None:
                continue
            for action in actions:
                child = self.child_node(self.problem, node, action)
                if child.state in self.explored or self.state_in(self.frontier, child.state):
                    pass
                    #if self.problem.goal_test(node.state) is True:
                    #    return self.solution(node)
                else:
                    self.frontier.append(child)
                    #self.frontier.sort(key=lambda x: (x.path_cost, x.state))

class Depth_First_Search:
    class d_node:
        def __init__(self, state, path_cost=0):
            self.state = state
            self.path_cost = path_cost

        def __name__():
            return self.state

        def __meta__():
            return self.state

    def solution(self, node):
        return (node.state, (node.path_cost + self.problem.start_time) % 24) if node is not None else None

    def __init__(self):
        self.frontier = []
        self.explored = set()

    def child_node(self, problem, node, action):
        return self.d_node(action, node.path_cost + 1)

    def state_in(self, state):
        for node in self.frontier:
            if state == node.state:
                return True
        return False

    def do_search(self, problem):
        self.problem = problem
        node = self.d_node(problem.initial_state)
        self.frontier = [node]
        self.explored = set()

        while(True):
            if not self.frontier:
                return None
            #debugLines.append("-------``")
            #for deitem in self.frontier:
                #debugLines.append(deitem.state)
            #debugLines.append("``=======")
            node = self.frontier.pop()

            #debugLines.append(node.state)
            if self.problem.goal_test(node.state):
                return self.solution(node)
            self.explored.add(node.state)
            actions = self.problem.actions(node.state, search="DFS")
            if actions is None or self.explored.issuperset(set(actions)):
                continue
            else:
                for action in actions:
                    #if action not in self.explored and not self.state_in(action):
                    if action not in self.explored:# and not self.state_in(action):
                        self.frontier.append(self.d_node(action, node.path_cost + 1))

    def do_recursive_search(self, problem):
        self.problem = problem
        self.explored = set()
        return self.recursive_dfs(self.d_node(problem.initial_state))

    def recursive_dfs(self, node):
        if self.problem.goal_test(node.state):
            return self.solution(node)

        self.explored.add(node.state)
        actions = problem.actions(node.state, search="DFS")
        if actions is None:
            return None
        actions.reverse()
        for action in actions:
            if action not in self.explored:
                child = self.child_node(problem, node, action)
                result = self.recursive_dfs(child)
                if result is not None:
                    return result
        return None




if __name__=='__main__':
    inputFileName = sys.argv[2]
    with open(inputFileName) as inputFile:
        outFile = file("output.txt", 'w')

        debugFile = file("debug.txt", "w")
        #debugLines = []

        lines = inputFile.readlines()
        line_num = len(lines)
        progress = 0
        counter = 0
        for i in range(line_num):
            if lines[i].isspace() is True:
                pass
            else:
                #debugLines.append(lines[i])
                #print(lines[i])
                if progress is 0:
                    #print("Progress 0")
                    tasks = int(lines[i])
                    progress = 1
                elif progress is 1:
                    counter += 1
                    #debugLines.append("---------")
                    #debugLines.append("--" + str(counter) + "--")
                 #   print(counter)
                    #print("Progress 1")
                    task_type = lines[i].split()[0]
                    progress = 2
                elif progress is 2:
                    #print("Progress 2")
                    source = lines[i].split()[0]
                    progress = 3
                elif progress is 3:
                    #print("Progress 3")
                    dests = lines[i].split()
                    progress = 4
                elif progress is 4:
                    #print("Progress 4")
                    middles = lines[i].split()
                    progress = 5
                elif progress is 5:
                    #print("Progress 5")
                    pipe_num = int(lines[i])
                    pipe_counter = 0
                    pipes = []
                    if pipe_num == 0:
                        progress = 7
                    else:
                        progress = 6
                elif progress is 6:
                    #print("Progress 6")
                    pipes.append(lines[i].split())
                    pipe_counter += 1
                    if pipe_counter == pipe_num:
                        progress = 7
                elif progress is 7:
                    #print("Progress 7")
                    start_time = int(lines[i])
                    if task_type == "BFS":
                        problem = Problem(source, dests, pipes, start_time)
                        bfs = Breadth_First_Search()
                        solution = bfs.do_search(problem)
                        if solution is not None:
                            outFile.write(solution[0] + " " + str(solution[1])+ "\n")
                        else:
                            outFile.write("None\n")
                        #print(solution)
                    elif task_type == "DFS":
                        problem = Problem(source, dests, pipes, start_time)
                        dfs = Depth_First_Search()
                        #solution1 = dfs.do_search(problem)
                        solution2 = dfs.do_recursive_search(problem)
                        #print("DFS result compare")
                        #print(solution1 == solution2)
                        #print("end of DFS result compare")
                        solution = solution2
                        if solution is not None:
                            outFile.write(solution[0] + " " + str(solution[1])+ "\n")
                        else:
                            outFile.write("None\n")
                        #print("==")
                        #print(solution)
                    elif task_type == "UCS":
                        problem = Problem(source, dests, pipes, start_time)
                        ucs = Uniform_Cost_Search()
                        solution = ucs.do_search(problem)
                        if solution is not None:
                            outFile.write(solution[0] + " " + str(solution[1])+ "\n")
                        else:
                            outFile.write("None\n")
                        pass
                    #implement BFS and DFS here
                    # and output here
                    progress = 1
                    #debugLines.append("================")
                    #for li in debugLines:
                    #    debugFile.write(li + "\n")
                    #debugLines = []
                #print(lines[i])
                #print("-----------------------------")

