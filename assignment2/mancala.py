import sys
import copy

class mancala:
    def __init__(self, player,
            boardUp, boardDown, mancalaLeft, mancalaRight):
        self.player = player
        self.depth = 0
        boardUp.reverse()
        boardUp.append(mancalaLeft)
        boardDown.append(mancalaRight)
        board = (boardDown, boardUp)
        self.board = board

    def get_player(self):
        return self.player
    def get_opponent(self):
        return 2 if self.player == 1 else 1
    def get_player_note(self):
        return "B" if self.player == 1 else "A"
    def get_opponent_note(self):
        return "A" if self.player == 1 else "B"

    def get_clone(self):
        return copy.deepcopy(self)
    def get_board_len(self):
        return len(self.board[0])
    def get_board_size(self):
        return len(self.board[0]) * 2
    def get_actions(self, player):
        actions = []
        if player == self.player:
            for i in range(self.get_board_len() - 1):
                if self.get_self_board()[i] != 0:
                    actions.append(i)
        else:
            for i in range(self.get_board_len() - 1):
                if self.get_opponent_board()[i] != 0:
                    actions.append(i)
        if player == 2:
            actions.reverse()
        return actions

    def get_board(self, choice=0):
        if self.player == 1:
            (selfboard, opponentboard) = self.board
        if self.player == 2:
            (opponentboard, selfboard) = self.board
        if choice == 0:
            return selfboard
        else:
            return opponentboard

    def get_self_board(self):
        return self.get_board(0)
    def get_opponent_board(self):
        return self.get_board(1)
    def move(self, move, player):
        # return mancala after the move
        # and if it still can move for one more step
        # move: the index of selfboard or opponentboard
        # player: the player that picks the step
        # Local View:
        # index: 7 6 5 4 3 2 1 0 player2
        #        m p p p p p p p
        #        p p p p p p p m
        # index: 0 1 2 3 4 5 6 7 player1
        # m is mancala and p is pits
        # Notice the global view is:
        # index: 1 2 3 4 5 6 7 8 9 player2
        #        m p p p p p p p m
        #        m p p p p p p p m
        # index: 1 2 3 4 5 6 7 8 9 player1

        mancala_after = self.get_clone()
        more_step = 0
        if player == self.player:
            selfboard = mancala_after.get_self_board()
            opponentboard = mancala_after.get_opponent_board()
        else:
            selfboard = mancala_after.get_opponent_board()
            opponentboard = mancala_after.get_self_board()
        stones = selfboard[move]

        selfboard[move] = 0
        average_up = stones / (mancala_after.get_board_size() - 1)
        remaining_up = stones % (mancala_after.get_board_size() - 1)
        for i in range(mancala_after.get_board_len()):
            selfboard[i] += average_up
        for i in range(mancala_after.get_board_len() - 1):
            opponentboard[i] += average_up

        if average_up == 1 and remaining_up == 0:
            collect_sum = selfboard[move] + opponentboard[mancala_after.get_board_len() - 2 - move]
            selfboard[move] = 0
            opponentboard[mancala_after.get_board_len() - 2 - move] = 0
            selfboard[-1] += collect_sum


        for i in range(1, remaining_up + 1):
            position = i + move
            if position < mancala_after.get_board_len():
                selfboard[position] += 1
            elif position >= mancala_after.get_board_len() and\
                 position < mancala_after.get_board_size() - 1:
                opponentboard[position % mancala_after.get_board_len()] += 1
            else:
                selfboard[position % (mancala_after.get_board_size() - 1)] += 1

            if i == remaining_up :# last loop and get into an empty place
                #print(position)
                #print(mancala_after.get_board_size() - 1)
                if position == mancala_after.get_board_len() - 1:
                    more_step = 1

#if self is empty... then we need to pick it up?
                if position <= mancala_after.get_board_len() - 1:
                    if selfboard[position] == 1 and position < mancala_after.get_board_len() - 1:
                        opponent_position = mancala_after.get_board_len() - 2 - position
                        collectStones = selfboard[position] + opponentboard[opponent_position]
                        selfboard[position] = 0
                        opponentboard[opponent_position] = 0
                        selfboard[-1] += collectStones
                elif position >= (mancala_after.get_board_size() - 1):
                    #print("ENTER!")
                    position = position % (mancala_after.get_board_size() - 1)
                    if selfboard[position] == 1 and position < mancala_after.get_board_len() - 1:
                        opponent_position = mancala_after.get_board_len() - 2 - position
                        collectStones = selfboard[position] + opponentboard[opponent_position]
                        selfboard[position] = 0
                        opponentboard[opponent_position] = 0
                        selfboard[-1] += collectStones
        #raw_input()

            # After all
        if mancala_after.if_end_game():
            more_step = 0
            selfsum = 0
            opponentsum = 0
            for i in range(mancala_after.get_board_len()):
                selfsum += selfboard[i]
                selfboard[i] = 0
                opponentsum += opponentboard[i]
                opponentboard[i] = 0
            selfboard[-1] = selfsum
            opponentboard[-1] = opponentsum

        return (mancala_after, more_step)

    def if_end_game(self):
        if(self.get_actions(self.get_player()) == [] or\
           self.get_actions(self.get_opponent()) == []):
            return True
        else:
            return False


    def get_move_name(self, action, player):
        if player == 1:
            return "B" + str(action+2)
        if player == 2:
            return "A" + str(self.get_board_len() - action)

    def print_board(self):
        mancala_print = self.get_clone()
        if self.player == 1:
            boardUp = mancala_print.get_opponent_board()
            boardDown = mancala_print.get_self_board()
        else:
            boardUp = mancala_print.get_self_board()
            boardDown = mancala_print.get_opponent_board()
        boardUp.reverse()
        print("---------")
        print(boardUp)
        print(boardDown)
        print("=========")


    def output_to_file(self, filename):
        if self.player == 1:
            boardUp = self.get_opponent_board()
            boardDown = self.get_self_board()
        else:
            boardUp = self.get_self_board()
            boardDown = self.get_opponent_board()

        boardUp.reverse()
        with open(filename, 'w') as f:
            for i in range(1, self.get_board_len()):
                f.write(str(boardUp[i]))
                if i != self.get_board_len() - 1:
                    f.write(" ")
                else:
                    f.write('\n')
            for i in range(0, self.get_board_len() - 1):
                f.write(str(boardDown[i]))
                if i != self.get_board_len() - 2:
                    f.write(" ")
                else:
                    f.write('\n')
            f.write(str(boardUp[0]))
            f.write('\n')
            f.write(str(boardDown[-1]))
            f.write('\n')

    def terminal_test(self, cutoff):
        return self.depth >= cutoff or self.if_end_game()

    def combo_test(self, player):
        for a in range(self.get_board_len() - 1):
            if self.combo_action_test(player, a) == True:
                return True

        return False

    def combo_action_test(self, player, a):
        if player == "max":
            selfboard = self.get_self_board()
            stones = selfboard[a]
            position = a + stones + 1
            if (position % (self.get_board_size() - 1))  == self.get_board_len():
                return True
        if player == "min":
            opponentboard = self.get_self_board()
            stones = opponentboard[a]
            position = a + stones + 1
            if (position % (self.get_board_size() - 1))  == self.get_board_len():
                return True

        return False

class strategy:
    def __init__(self, task, cutoff):
        self.taskNum = task
        self.cutoff = cutoff
        self.max_pair = ("-Infinity", None)
        self.enable_traverse_log = 1
        self.counter = 0
        self.pauseFlag = 0

    def process_mancale(self, mancala, outputFileName):
        if self.taskNum is 1:
            next_mancala = self.greedy(mancala)
        elif self.taskNum is 2:
            next_mancala = self.minimax(mancala)
        elif self.taskNum is 3:
            next_mancala = self.alpha_beta(mancala)
        elif self.taskNum is 4:
            next_mancala = self.competition(mancala)
        else:
            next_mancala = self.greedy(mancala)

        # If the game ends and did not collect stones.
        # Remember to do it here
        if next_mancala == None: #Could not do any step
            mancala.output_to_file(outputFileName)
        else:
            next_mancala = next_mancala.output_to_file(outputFileName)

    def greedy(self, mancala):
        self.cutoff = 1
        self.enable_traverse_log = 0
        return self.minimax(mancala)
        pass
    def minimax(self, mancala):
        if self.enable_traverse_log == 1:
            self.traverse_log = open("traverse_log.txt", "w")
            self.traverse_log.write("Node,Depth,Value\n")
        self.max_val(mancala, "root")
        return self.max_pair[1]
        pass
    def alpha_beta(self, mancala):
        self.traverse_log = open("traverse_log.txt", "w")
        self.traverse_log.write("Node,Depth,Value,Alpha,Beta\n")
        self.ab_max_val(mancala, "root", "-Infinity", "Infinity")
        return self.max_pair[1]
        pass
    def competition(self, mancala):
        pass
    def heuristic(self, mancala):
        return mancala.get_self_board()[-1] - mancala.get_opponent_board()[-1]

    def ab_min_val(self, mancala, move_name, alpha, beta):
        if mancala.terminal_test(self.cutoff) and move_name[0] != mancala.get_opponent_note():
            v = self.heuristic(mancala)
            self.counter += 1
            if self.counter == 0 + 00:
                self.pauseFlag = 1
            if self.pauseFlag == 1:
                print(move_name)
                mancala.print_board()
                raw_input()
            self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
        else:
            v = "Infinity"
            self.counter += 1
            if self.counter == 0 + 00:
                self.pauseFlag = 1
            if self.pauseFlag == 1:
                print(move_name)
                mancala.print_board()
                raw_input()
            self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
        actions = mancala.get_actions(mancala.get_opponent())
        for a in actions:

            more_step = mancala.combo_action_test("min", a)
            tmpdepth = mancala.depth
            if move_name[0] == mancala.get_player_note():
                tmpdepth = mancala.depth + 1
            if tmpdepth > self.cutoff:
                continue

            (new_mancala, more_step) = mancala.move(a, mancala.get_opponent())

            new_mancala.depth = tmpdepth

            if more_step == 0:
                v = self.ab_max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_opponent()), alpha, beta)\
                        if v == "Infinity" \
                        else min(v, self.ab_max_val(new_mancala, new_mancala.get_move_name(a,
                            new_mancala.get_opponent()), alpha, beta))

            else:
                v = self.ab_min_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_opponent()), alpha, beta) \
                        if v == "Infinity" \
                        else min(v, self.ab_min_val(new_mancala, new_mancala.get_move_name(a,
                            new_mancala.get_opponent()), alpha, beta))


            if v != "Infinity" and alpha != "-Infinity" and v <= alpha:
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
                return v
            if beta == "Infinity":
                beta = v
            elif beta != "Infinity" and v != "Infinity":
                beta = min(beta, v)

            self.counter += 1
            if self.counter == 0 + 00:
                self.pauseFlag = 1
            if self.pauseFlag == 1:
                print(move_name)
                mancala.print_board()
                raw_input()
            self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
        return v

    def ab_max_val(self, mancala, move_name, alpha, beta):
        if mancala.terminal_test(self.cutoff) and move_name[0] != mancala.get_player_note():
            v = self.heuristic(mancala)
            self.counter += 1
            if self.counter == 0 + 00:
                self.pauseFlag = 1
            if self.pauseFlag == 1:
                print(move_name)
                mancala.print_board()
                raw_input()
            self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
        else:
            v = "-Infinity"
            self.counter += 1
            if self.counter == 0 + 00:
                self.pauseFlag = 1
            if self.pauseFlag == 1:
                print(move_name)
                mancala.print_board()
                raw_input()
            self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
        actions = mancala.get_actions(mancala.get_player())
        for a in actions:

            more_step = mancala.combo_action_test("max", a)
            tmpdepth = mancala.depth
            if move_name[0] == mancala.get_opponent_note() or move_name == "root":
                tmpdepth = mancala.depth + 1
            if tmpdepth > self.cutoff:
                continue

            (new_mancala, more_step) = mancala.move(a, mancala.get_player())

            new_mancala.depth = tmpdepth

            if more_step == 0:
                v = self.ab_min_val(new_mancala, new_mancala.get_move_name(a, mancala.get_player()), alpha, beta) \
                        if v == "-Infinity" \
                        else max(v, self.ab_min_val(new_mancala, new_mancala.get_move_name(a, mancala.get_player()),
                            alpha, beta))

                if new_mancala.depth == 1 and \
                    (self.max_pair[0] == "-Infinity" or v > self.max_pair[0]):
                        self.max_pair = (v, new_mancala)
            else:
                v = self.ab_max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_player()), alpha, beta) \
                        if v == "-Infinity" \
                        else max(v, self.ab_max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_player()),
                            alpha, beta))

            if beta != "Infinity" and v != "-Infinity" and v >= beta:
                
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
                return v

            if alpha == "-Infinity":
                alpha = v
            elif alpha != "-Infinity" and v != "-Infinity":
                alpha = max(alpha, v)

            self.counter += 1
            if self.counter == 0 + 00:
                self.pauseFlag = 1
            if self.pauseFlag == 1:
                print(move_name)
                mancala.print_board()
                raw_input()
            self.traverse_log.write(move_name + "," + str(mancala.depth) + ","\
                    + str(v) + "," + str(alpha) + "," + str(beta) + "\n")
        return v

    def min_val(self, mancala, move_name):
        #print(mancala.terminal_test(self.cutoff))
        #print(mancala.combo_test("min"))
        #print("min")
        if mancala.terminal_test(self.cutoff) and move_name[0] != mancala.get_opponent_note():
            v = self.heuristic(mancala)
            if self.enable_traverse_log == 1:
                self.counter += 1
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + "," + str(v) + "\n")
        else:
            v = "Infinity"
            if self.enable_traverse_log == 1:
                self.counter += 1
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + "," + str(v) + "\n")
        actions = mancala.get_actions(mancala.get_opponent())
        for a in actions:

            more_step = mancala.combo_action_test("min", a)
            tmpdepth = mancala.depth
            if move_name[0] == mancala.get_player_note():
                tmpdepth = mancala.depth + 1
            if tmpdepth > self.cutoff:
                continue

            (new_mancala, more_step) = mancala.move(a, mancala.get_opponent())

            new_mancala.depth = tmpdepth

            if more_step == 0:
                v = self.max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_opponent()))\
                        if v == "Infinity" \
                        else min(v, self.max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_opponent())))

            else:
                v = self.min_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_opponent())) \
                        if v == "Infinity" \
                        else min(v, self.min_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_opponent())))
            if self.enable_traverse_log == 1:
                self.counter += 1
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + "," + str(v) + "\n")
        return v

    def max_val(self, mancala, move_name):
        #print(mancala.terminal_test(self.cutoff))
        #print(mancala.combo_test("max"))
        #print("max")
        if mancala.terminal_test(self.cutoff) and move_name[0] != mancala.get_player_note():
            v = self.heuristic(mancala)
            if self.enable_traverse_log == 1:
                self.counter += 1
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + "," + str(v) + "\n")
        else:
            v = "-Infinity"
            if self.enable_traverse_log == 1:
                self.counter += 1
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + "," + str(v) + "\n")
        actions = mancala.get_actions(mancala.get_player())
        for a in actions:

            more_step = mancala.combo_action_test("max", a)
            tmpdepth = mancala.depth
            if move_name[0] == mancala.get_opponent_note() or move_name == "root":
                tmpdepth = mancala.depth + 1
            if tmpdepth > self.cutoff:
                continue

            (new_mancala, more_step) = mancala.move(a, mancala.get_player())

            new_mancala.depth = tmpdepth

            if more_step == 0:
                v = self.min_val(new_mancala, new_mancala.get_move_name(a, mancala.get_player())) \
                        if v == "-Infinity" \
                        else max(v, self.min_val(new_mancala, new_mancala.get_move_name(a, mancala.get_player())))
                if new_mancala.depth == 1 and \
                    (self.max_pair[0] == "-Infinity" or v > self.max_pair[0]):
                        self.max_pair = (v, new_mancala)
            else:
                v = self.max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_player())) \
                        if v == "-Infinity" \
                        else max(v, self.max_val(new_mancala, new_mancala.get_move_name(a, new_mancala.get_player())))
            if self.enable_traverse_log == 1:
                self.counter += 1
                if self.counter == 0 + 00:
                    self.pauseFlag = 1
                if self.pauseFlag == 1:
                    print(move_name)
                    mancala.print_board()
                    raw_input()
                self.traverse_log.write(move_name + "," + str(mancala.depth) + "," + str(v) + "\n")
        return v

    def heuristic_competition(self, mancala, move):
        pass

if __name__ == "__main__":
    inputFileName = sys.argv[2]
    outputFileName = "next_state.txt"
    with open(inputFileName) as f:
        lines = f.readlines()
        progress = 0
        for i in range(len(lines)):
            line = lines[i]
            if line.isspace():
                pass
            if progress is 0:
                task = int(line)
                progress = 1
            elif progress is 1:
                player = int(line)
                progress = 2
            elif progress is 2:
                cutoff = int(line)
                progress = 3
            elif progress is 3:
                board2 = [int(x) for x in line.split()]
                progress = 4
            elif progress is 4:
                board1 = [int(x) for x in line.split()]
                progress = 5
            elif progress is 5:
                mancala2 = int(line)
                progress = 6
            elif progress is 6:
                mancala1 = int(line)

    m = mancala(player, board2, board1, mancala2, mancala1)
    s = strategy(task, cutoff)
    s.process_mancale(m, outputFileName)
    pass

